import java.util.Scanner;

public class Lab1Bai4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Moi ban nhap he so cua phuong trinh bac 2: ");
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        double delta = Math.pow(b, 2) - 4 * a * c;
        if (delta < 0) System.out.println("Khong co gia tri can delta");
        else if (delta == 0) System.out.println("Can delta = 0");
        else System.out.println("Can delta = " + Math.sqrt(delta));
    }
}
