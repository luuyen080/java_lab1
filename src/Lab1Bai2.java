import java.util.Scanner;

public class Lab1Bai2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Moi ban nhap chieu dai: ");
        double chieuDai = scanner.nextDouble();
        System.out.print("Moi ban nhap chieu rong: ");
        double chieuRong = scanner.nextDouble();
        System.out.println("Chu vi cua hinh chu nhat la: " + 2 * (chieuDai + chieuRong));
        System.out.println("Dien tich hinh chu nhat la: " + chieuDai * chieuRong);
        System.out.println("Canh nho nhat cua hinh chu nhat la: " + Math.min(chieuDai, chieuRong));
    }
}
