import java.util.Scanner;

public class Lab1Bai1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Moi ban nhap ho ten: ");
        String hoTen = scanner.nextLine();
        System.out.print("Moi ban nhap diem: ");
        double diem = scanner.nextDouble();
        System.out.print(hoTen + " " + diem + " " + "diem" + "\n");
        System.out.printf("%s %.1f diem\n", hoTen, diem);
        System.out.println(hoTen + " " + diem + " " + "diem");
    }
}
