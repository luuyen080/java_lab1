import java.util.Scanner;

public class Lab1Bai3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Moi ban nhap canh cua khoi lap phuong: ");
        double canh = scanner.nextDouble();
        System.out.println("The tich cua khoi lap phuong la: " + Math.pow(canh, 3));
    }
}
